<div align="center">
  <h1>Template Method</h1>
</div>

<div align="center">
  <img src="template method_method_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Template Method is a behavioral pattern for defining an algorithm that lets subclasses
implement or
override specific steps.**

### Real-World Analogy

_Extensions of an architectural plan._

Each building step (hook) in the architectural plan (template method), such as laying the foundation, framing, building
walls,
installing plumbing etc., can be slightly changed by the owner to adjust the resulting house.

### Participants

- :bust_in_silhouette: **Interface**
    - Provides an interface to:
        - `method` object (aka `other_method`)
    - Optionally provides an interface to:
        - `method` object

- :man: **ConcreteObject**
    - ...

• AbstractClass (Application)- defines abstract primitive operations that concrete subclasses define to implement steps
of an algorithm.- implements a template method method defining the skeleton of an algorithm. The template method method
calls
primitive operations as well as operations defined in AbstractClass or those of other objects. • ConcreteClass (
MyApplication)- implements the primitive operations to carry out subclass-specific steps of the algorithm

### Collaborations

...
ConcreteClass relies on AbstractClass to implement the invariant steps ofthe
algorithm

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When ... .**

to implement the invariant parts of an algorithm once and leave it up to subclasses to implement the behavior that can
vary. • when common behavior among subclasses should be factored and localized in a common class to avoid code
duplication. This is a good example of "refactoring to generalize" as described by Opdyke and Johnson [OJ93]. You first
identify the differences in the existing code and then separate the differences into new operations. Finally, you
replace the differing code with a template method method that calls one of these new operations. • to control subclasses
extensions. You can define a template method method that calls "hook" operations (see Consequences) at specific points,
thereby
permitting extensions only at those points.

### Motivation

- ...

Consider an application framework that provides Application and Document
classes.TheApplication classisresponsible for opening existing documents stored
in an externalformat, such as a file. A Document object represents the information
in a document once it's read from the file.

Applications built with the framework can subclass Application and Document to
suit specific needs. For example, a drawing application defines DrawApplication
and DrawDocument subclasses; a spreadsheet application defines SpreadsheetApplication and SpreadsheetDocument
subclasses.

The abstract Application class defines the algorithm for opening and reading a
document in its OpenDocument operation:

OpenDocument defines each step for opening a document. It checks if the document can be opened, creates the
application-specific Document object, adds it to
its set of documents, and reads the Document from a file.

We call OpenDocument a template method. A template method defines an algorithm in terms of abstract operations that
subclasses override to provide concrete
behavior. Application subclasses define the steps of the algorithm that check if
the document can be opened (CanOpenDocument) and that create the Document
(DoCreateDocument). Document classes define the step that reads the document
(DoRead). The template method also defines an operation that lets Application
subclasses know when the document is about to be opened (AboutToOpenDocument), in case they care.

By defining some of the steps of an algorithm using abstract operations, the template method fixes their ordering, but
it lets Application and Document subclasses
vary those steps to suit their needs.

---

Use the Template Method pattern when you want to let clients extend only particular steps of an algorithm, but not the
whole algorithm or its structure.

The Template Method lets you turn a monolithic algorithm into a series of individual steps which can be easily extended
by subclasses while keeping intact the structure defined in a superclass.

Use the pattern when you have several classes that contain almost identical algorithms with some minor differences. As a
result, you might need to modify all classes when the algorithm changes.

When you turn such an algorithm into a template method, you can also pull up the steps with similar implementations into
a superclass, eliminating code duplication. Code that varies between subclasses can remain in subclasses.

### Known Uses

- ...

Algorithm Frameworks: Creating frameworks where the overall structure of an algorithm is defined in a base class and
specific steps are implemented in derived classes. For example, in game development, defining the basic game loop in a
base class and letting specific games implement their unique behaviors.

Software Libraries: Providing a template method for common tasks in libraries, allowing users to override specific steps
for customization. For instance, in a library for data manipulation, providing a template method for data processing but
allowing users to implement specific data transformations.

Document Processing: When creating documents with similar structures but differing content or formatting, such as
generating reports in different formats (PDF, HTML, etc.), the template method can define the overall structure while
subclasses implement specific output formats.

Web Development: Implementing web application frameworks where the basic structure of handling requests is defined in a
base class and specific request handling is implemented in derived classes.

Software Development Lifecycle: In software development methodologies, the template method can be used in defining
standardized steps for development processes (like code reviews, testing procedures) across projects, with specific
implementations varying based on project requirements.

Automated Testing: Implementing test frameworks where the overall testing process is defined in a base class, and
specific test cases or steps are implemented in subclasses.

Networking: In networking protocols, the template method can define the structure of how messages are sent or received,
with specific implementations handling the details of the communication protocol.

UI Development: Creating user interface frameworks where the overall layout or structure of components is defined in a
base class, while specific behaviors or interactions are implemented in subclasses. For instance, defining the structure
of a window or a dialog box and letting subclasses implement specific UI elements.

Database Access: In database-related operations, the template method can be employed to define a standardized way to
interact with different databases (SQL, NoSQL), with specific implementations for connecting, querying, and handling
results.

Logging: Implementing logging frameworks where the basic logging process is defined in a base class (such as open log,
write log, close log), allowing different log formats or destinations to be handled by subclasses.

Concurrency Control: In multi-threaded applications, providing a template method to control the sequence of execution or
synchronization mechanisms while allowing subclasses to specify certain behavior for individual threads.

Resource Management: Implementing resource management frameworks where the acquisition and release of resources follow a
standard pattern in a base class, but specific resource types or handling methods are implemented by subclasses.

Cryptography: Defining cryptographic algorithms where the overall encryption or decryption process is outlined in a base
class, while specific implementations for different encryption standards or algorithms are provided by subclasses.

Workflow Systems: Implementing workflow systems where the general steps or stages of a process are defined in a base
class, but the specific actions or tasks at each stage are implemented by subclasses.

Teaching Platforms: Creating educational platforms where the structure of a lesson or course is outlined in a base
class, while the content or specific exercises are implemented by subclasses.

All non-abstract methods of java.io.InputStream, java.io.OutputStream, java.io.Reader and java.io.Writer.
All non-abstract methods of java.util.AbstractList, java.util.AbstractSet and java.util.AbstractMap.
javax.servlet.http.HttpServlet, all the doXXX() methods by default sends a HTTP 405 "Method Not Allowed" error to the
response. You're free to implement none or any of them.

Template methods are so fundamental that they can be found in almost every
abstract class. Wirfs-Brock et al. [WBWW90, WBJ90] provide a good overview
and discussion of template methods

### Categorization

Purpose:  **Behavioral**  
Scope:    **Class**   
Mechanisms: **Inheritance**

Behavioral patterns are concerned with algorithms and the assignment of responsibilities between objects.
Behavioral patterns describe not just patterns of objects or classes
but also the patterns of communication between them.

Behavioral class patterns use inheritance to distribute behavior between classes. This chapter includes two such
patterns.

Behavioral object patterns use object composition rather than inheritance.
Some describe how a group of peer objects cooperate to perform a task that no single object can carry out by itself.
An important issue here is how peer objects know about each other.
Peers could maintain explicit references to each other, but that would increase their coupling.
Some patterns provide indirection to allow loose coupling
mediator, chain of responsibility, observer
Other behavioral object patterns are concerned with encapsulating behavior in an object and delegating requests to it.
strategy, command, state, visitor, iterator

### Aspects that can vary

- Steps of an algorithm.

### Solution to causes of redesign

- Algorithmic dependencies.
    - Algorithms are often extended, optimized, and replaced, forcing dependants to also change.

### Consequences

| Advantages                                         | Disadvantages                       |
|----------------------------------------------------|-------------------------------------|
| :heavy_check_mark: **Short description.** <br> ... | :x: **Short description.** <br> ... |

Template methods are a fundamental technique for code reuse. They are particularly important in class libraries,
because they are the means for factoring out
common behavior in library classes.
Template methods lead to an inverted control structure that'ssometimes referred
to as "the Hollywood principle," that is, "Don't call us, we'll call you" [Swe85].
This refers to how a parent class calls the operations of a subclass and not the
other way around.
Template methods call the following kinds of operations:
• concrete operations (either on the ConcreteClass or on client classes);
• concrete AbstractClass operations (i.e., operations that are generally useful
to subclasses);
• primitive operations (i.e., abstract operations);
• factory methods (see Factory Method (107)); and
• hook operations, which provide default behavior that subclasses can extend
if necessary. A hook operation often does nothing by default.
It's important for template methods to specify which operations are hooks (may
be overridden) and which are abstract operations (must be overridden). To reuse
an abstract class effectively, subclass writers must understand which operations
are designed for overriding.
A subclass can extend a parent class operation's behavior by overriding the operation and calling the parent operation
explicitly:
void DerivedClass::Operation () {
ParentClass::Operation();
// DerivedClass extended behavior
}
Unfortunately, it's easy to forget to call the inherited operation. Wecantransform
such an operation into a template method to give the parent control over how
subclasses extend it. The idea is to call a hook operation from a template method
in the parent class. Then subclasses can then override this hook operation:
void ParentClass::Operation () {
// ParentClass behavior
HookOperation();
}
HookOperation does nothing in ParentClass:
void ParentClass::HookOperation () { }
Subclasses override HookOperation to extend its behavior:
void DerivedClass::HookOperation () {
// derived class extension
}

You can let clients override only certain parts of a large algorithm, making them less affected by changes that happen
to other parts of the algorithm.
You can pull the duplicate code into a superclass.

Some clients may be limited by the provided skeleton of an algorithm.
You might violate the Liskov Substitution Principle by suppressing a default step implementation via a subclass.
Template methods tend to be harder to maintain the more steps they have.

### Relations with Other Patterns

_Distinction from other patterns:_
_Combination with other patterns:_

- ...

Factory Methods (107) are often called by template methods. In the Motivation
example, the factory method DoCreateDocument is called by the template method
OpenDocument.
Strategy (315): Template methods use inheritance to vary part of an algorithm.
Strategies use delegation to vary the entire algorithm

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **The pattern can be implemented by ...**

Template Method Method can be implemented by a method in base class that calls a bunch of other methods that are either
abstract or empty.

(recognizable by behavioral methods which already have a "default" behaviour defined by an abstract type)

### Structure

```mermaid
classDiagram
    class AbstractClass {
        <<abstract_baseclass>>
        + templateMethod(): void
        # primitiveOperation1(): void
        # primitiveOperation2(): void
    }

    class ConcreteClass {
        + primitiveOperation1(): void
        + primitiveOperation2(): void
    }

    AbstractClass <|-- ConcreteClass: extends

```

### Variations

_Variation name:_

- **VariationA**: ...
    - :heavy_check_mark: ...
    - :x: ...
- **VariationB**: ...
    - :heavy_check_mark: ...
    - :x: ...

Three implementation issues are worth noting:

1. Using C++ access control. In C++, the primitive operations that a template
   method calls can be declared protected members. This ensures that they
   are only called by the template method. Primitive operations that must be overridden are declared pure virtual. The
   template method itself should not
   be overridden; therefore you can make the template method a nonvirtual
   member function.
2. Minimizing primitive operations. An important goal in designing template
   methods is to minimize the number of primitive operations that a subclass
   must override to flesh out the algorithm. The more operations that need
   overriding, the more tedious things get for clients.
3. Naming conventions. You can identify the operations that should be overrid-
   den by adding a prefix to their names. For example, the MacApp framework
   for Macintosh applications [App89] prefixes template method names with
   "Do-": "DoCreateDocument", "DoRead", and so forth.

### Implementation

In the example we apply the template method pattern to a ... system.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Template Method](https://refactoring.guru/design-patterns/template-method)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [ArjanCodes: Two UNDERRATED Design Patterns Write BETTER PYTHON CODE Part 6](https://youtu.be/t0mCrXHsLbI?si=hhm7vunxCaWAsVzU)
- [Geekifit: The Template Method Pattern Explained Implemented in Java](https://youtu.be/cGoVDzHvD4A?si=fu7nDHx4YVJP5C3q)
- [Eric Tech: Template Method Pattern](https://youtu.be/pIs5BQBrE70?si=x_OoVbkPI-vE1miJ)

<br>
<br>
