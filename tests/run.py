import subprocess
import sys


def all_tests() -> None:
    execute(["python", "-u", "-m", "unittest", "discover"])


def coverage_report() -> None:
    execute(["poetry", "run", "coverage", "run", "-m", "unittest", "discover", "tests"])
    execute(["poetry", "run", "coverage", "report"])


def coverage_html() -> None:
    execute(["poetry", "run", "coverage", "run", "-m", "unittest", "discover", "tests"])
    execute(["poetry", "run", "coverage", "html"])


def execute(command: list[str]) -> None:
    result = subprocess.run(command, capture_output=True, check=False)
    print(result.stdout.decode("utf8"), end="")
    print(result.stderr.decode("utf8"), end="")
    if result.returncode != 0:
        sys.exit(result.returncode)
